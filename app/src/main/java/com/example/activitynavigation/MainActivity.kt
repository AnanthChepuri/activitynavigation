package com.example.activitynavigation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var messageEditText: EditText = findViewById(R.id.messageEditText)
        var navigateToSecondActivityBtn: Button = findViewById(R.id.navigateToSecondActivityBtn)
        var selectTheAppToShareBtn: Button = findViewById(R.id.selectTheAppToShareBtn)

        navigateToSecondActivityBtn.setOnClickListener {
            Toast.makeText(this, messageEditText.text.toString(), Toast.LENGTH_SHORT).show()
            val intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("user_message", messageEditText.text.toString())
            startActivity(intent)
        }

        selectTheAppToShareBtn.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_SEND
            intent.putExtra(Intent.EXTRA_TEXT, messageEditText.text.toString())
            intent.type = "text/plain"

            startActivity(Intent.createChooser(intent, "Select the App to Share..."))
        }
    }
}
