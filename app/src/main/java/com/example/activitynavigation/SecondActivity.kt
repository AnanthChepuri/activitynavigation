package com.example.activitynavigation

import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        var msgEditText: TextView = findViewById(R.id.msgEditText)

        val msg = intent.extras?.getString("user_message")
        msgEditText.text = msg
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
